feat_categories = {'device_activity', 'userIpMap', 'device_location', 'clickActivity', 'device_app_info',
                   'campaign_activity', 'deviceAppInfo', 'user_ip_map', 'Inventory', 'inventory',
                   'deviceActivity', 'Dmp', 'campaignActivity', 'deviceLocation', 'dmp'}

posTag_v1 = ['dmp/flags/a:device_campaign_activity:action:Game:app_others',
             'dmp/flags/a:device_campaign_activity:action:Game:app_tutorial',
             'dmp/flags/a:device_campaign_activity:action:Game:app_open',
             'dmp/flags/a:device_campaign_activity:action:Game:app_purchase_with_revenue',
             'dmp/flags/a:device_campaign_activity:action:Game:app_install']
posTag_v2 = ['device_app_info/ios_genre_count/7017',
             'device_app_info/ios_genre_count/7001',
             'device_app_info/ios_genre_count/7014',
             'device_app_info/ios_genre_count/7015',
             'device_app_info/ios_genre_count/7002',
             'device_app_info/ios_primary_genre_count/6014',
             'device_app_info/ios_genre_count/6014']

posTag_kaka = ['device_app_info/ios_genre_count/7002',
               'device_app_info/ios_genre_count/7014']

posTag_v3 = ['device_app_info/ios_app_count/1462877149']

posTag_v4 = ['campaign_activity/group_majoraction_count/Game/app_install',
             'campaign_activity/group_action_count/Game/app_install']

posTag_v5 = ['campaign_activity/group_click_count/Game: RPG',
             'campaign_activity/group_click_count/Lottery & Gambling',
             'campaign_activity/group_click_count/Game']

posTag_v6 = ['dmp/flags/a:device_campaign_activity:click:Game: RPG',
             'dmp/flags/a:device_campaign_activity:click:Lottery & Gambling',
             'dmp/flags/a:device_campaign_activity:click:Game',
             'dmp/flags/a:device_campaign_activity:click:Utility',
             'dmp/flags/a:device_browse:browse_more_than_three_domains',
             'dmp/flags/a:D0102']

posTag_v7 = ['dmp/flags/a:device_campaign_activity:action:Game:app_others',
             'dmp/flags/a:device_campaign_activity:action:Game:app_tutorial',
             'dmp/flags/a:device_campaign_activity:action:Game:app_open',
             'dmp/flags/a:device_campaign_activity:action:Game:app_purchase_with_revenue',
             'dmp/flags/a:device_campaign_activity:action:Game:app_install',
             'device_app_info/ios_genre_count/7017',
             'device_app_info/ios_genre_count/7001',
             'device_app_info/ios_genre_count/7014',
             'device_app_info/ios_genre_count/7015',
             'device_app_info/ios_genre_count/7002',
             'device_app_info/ios_primary_genre_count/6014',
             'device_app_info/ios_genre_count/6014',
             'dmp/flags/a:device_campaign_activity:click:Game: RPG',
             'dmp/flags/a:device_campaign_activity:click:Lottery & Gambling',
             'dmp/flags/a:device_campaign_activity:click:Game',
             'dmp/flags/a:device_campaign_activity:click:Utility',
             'dmp/flags/a:device_browse:browse_more_than_three_domains',
             'dmp/flags/a:D0102']


posTag_comic = ['device_app_info/ios_app_count/939480693']
posTag_genre = ['device_app_info/ios_genre_count/6016',
                'device_app_info/ios_genre_count/6005',
                'device_app_info/ios_primary_genre_count/6014',
                'device_app_info/ios_genre_count/6018']