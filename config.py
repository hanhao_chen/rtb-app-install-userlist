import json
import logging
import os.path


class Config:

    def __init__(self, config_file, default_level=logging.INFO, logging_levels=None):
        if not os.path.exists(config_file):
            raise IOError('configuration file {} not found'.format(config_file))
        (self.idash_cfg, self.spark_cfg, self.local_folders,
         self.ulist_cfg, self.mysql_cfg) = self._read_config(config_file)
        self.default_level = default_level
        self.logging_levels = logging_levels

    def get_logger(self, module_name):
        lvl = (self.logging_levels.get(module_name) if self.logging_levels else None) or self.default_level
        ch = logging.StreamHandler()
        fmt = logging.Formatter(
            '[%(process)d][%(levelname)s] %(asctime)s - %(name)s.%(funcName)s:%(lineno)d - %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S')
        ch.setFormatter(fmt)
        logger = logging.getLogger(module_name)
        logger.setLevel(lvl)
        logger.addHandler(ch)
        return logger

    def _read_config(self, config_file):
        with open(config_file, 'r') as f:
            data = json.loads(f.read())
            return (data.get('idash', {}), data.get('spark', {}), data.get('local_folders', {}),
                    data.get('list_server'), data.get('mysql'))

    def get_spark_credentials(self):
        return self.spark_cfg['host'], self.spark_cfg['username'], self.spark_cfg['password']

    def get_idash_credentials(self):
        return self.idash_cfg['host'], self.idash_cfg['username'], self.idash_cfg['password']

    def get_local_folders(self):
        return self.local_folders

    def get_list_server_credentials(self):
        return self.ulist_cfg['host']

    def get_mysql_database_credentials(self, db_name):
        return self.mysql_cfg[db_name]
