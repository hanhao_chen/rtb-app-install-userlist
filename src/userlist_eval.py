import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from utils.data_utils import *
from utils.logging_utils import logger

LOGGER = logger(__name__)


def get_alls(userlists, labels, name, batch_size=10**5):
    size_recalls = [
        list(get_size_recall(userlist, batch_size=batch_size))
        for userlist in userlists
    ]
    plt.figure(figsize = (16,8))
    plt.subplot(121)
    for i in range(len(size_recalls)):
        size_recall = size_recalls[i]
        plt.plot(
            [x[0] for x in size_recall],
            [x[1] for x in size_recall],
            label = labels[i],
        )
    plt.legend(loc='best', shadow=True, fontsize='x-large')
    plt.ticklabel_format(useOffset=False, style='plain')
    plt.subplot(122)
    for i in range(len(size_recalls)):
        size_recall = size_recalls[i]
        plt.plot(
            [x[0] for x in size_recall],
            [x[1] / x[2] for x in size_recall],
            label = labels[i],
        )
    plt.legend(loc='best', shadow=True, fontsize='x-large')
    # plt.show()
    plt.ticklabel_format(useOffset=False, style='plain')
    plt.title('{}'.format(name))
    plt.savefig("{}.png".format(name))
    return size_recalls


def get_size_recall(userlist, batch_size = 10**5):#10**5):
    cu_size = 0 # cumulative size
    label = userlist['label'].values
    total_p = np.sum(label)
    cu_p = 0 # cumulative positive
    while True:
        yield (cu_size, cu_p / total_p, cu_size / userlist.shape[0], cu_p / cu_size if cu_size > 0 else np.nan)
        cu_size += batch_size
        if cu_size >= userlist.shape[0]:
            break
        cu_p += np.sum(label[range(cu_size - batch_size, cu_size)])


def all_action(df):
    return set(df['idfa'].values)

def all_action_ext(df):
    return set(df[df['is_rtb']==0]['idfa'].values)

def all_action_rtb(df):
    return set(df[df['is_rtb']==1]['idfa'].values)

def all_action_rv_rtb(df):
    return set(df[(df['is_rewarded']==1) & (df['is_rtb']==1)]['idfa'].values)

def all_action_inv_RD(df):
    return set(df[(df['is_rewarded']==1) & (df['bundle_id']=='1462877149')]['idfa'].values)

def all_action_inv_others(df):
    return set(df[(df['is_rewarded']==1) & (df['bundle_id']!='1462877149')]['idfa'].values)

def all_rt(df):
    return set(df['idfa'].values)

def main_jp():
    paths = ['jp/userlist_model_w1.parquet',
             'jp/userlist_model_w2.parquet',
             'jp/userlist_model_w1_showneg.parquet',
             'jp/userlist_model_w2_showneg.parquet',
             'jp/userlist_posTag_comic.parquet',
             'jp/userlist_posTag_genre.parquet']
    labels = ['model_w1',
              'model_w2',
              'model_w1_showneg',
              'model_w2_showneg',
              'posTag_comic',
              'posTag_genre']

    userlists = []
    for path in paths:
        userlists.append(pd.read_parquet(path))

    rt_df = pd.read_parquet('jp/rt_action/20200611_20200617')
    rt_idfa = rt_df['idfa'].values
    filter = load_filter_idfa('jp', 'ios', '10.0', '2020-06-04', dir='jp/bid/20200611_20200617')

    tmp_userlists = []
    for idx, userlist in enumerate(userlists):
        tmp_userlists.append(userlist[userlist['idfa'].isin(filter)].reset_index())

    act_idfa = rt_idfa
    for userlist in tmp_userlists:
        userlist['label'] = 0
        userlist.loc[userlist['idfa'].isin(act_idfa), 'label'] = 1
    size_recalls = get_alls(tmp_userlists, labels, name='rt_user-bid_user')

    for recall in size_recalls:
        print(recall[10])
    print('-------')

    rt_df = pd.read_parquet('jp/action/20200611_20200617')
    rt_idfa = rt_df[rt_df['vtcv']>0]['idfa'].values
    filter = load_filter_idfa('jp', 'ios', '10.0', '2020-06-04', dir='jp/show/20200611_20200617')

    tmp_userlists = []
    for idx, userlist in enumerate(userlists):
        tmp_userlists.append(userlist[userlist['idfa'].isin(filter)].reset_index())

    act_idfa = rt_idfa
    for userlist in tmp_userlists:
        userlist['label'] = 0
        userlist.loc[userlist['idfa'].isin(act_idfa), 'label'] = 1
    size_recalls = get_alls(tmp_userlists, labels, name='vtcv_user-show_user', batch_size=10**4)

    for recall in size_recalls:
        print(recall[5])
    print('-------')


def main():

    # model_userlist = 'tw/tw_oid=TwBO4vGqQI29z-2BshvrQA_fc7e89e1_2020-05-07_ml_userscore_train.snappy.parquet'
    idfa_filter = load_filter_idfa('tw', 'ios', '10.0', '2020-06-04', dir='tw/bid/20200611_20200617')
    # mdf = pd.read_parquet(model_userlist, columns=['idfa'])

    rdf1 = pd.read_parquet('tw/userlist_posTag_v1_64_610.parquet')
    rdf2 = pd.read_parquet('tw/userlist_posTag_v2_64_610.parquet')
    rdf3 = pd.read_parquet('tw/userlist_posTag_v3_64_610.parquet')
    rdf4 = pd.read_parquet('tw/userlist_posTag_v4_64_610.parquet')
    rdf7 = pd.read_parquet('tw/userlist_model.parquet')
    # rdf6 = pd.read_parquet('tw/userlist_posTag_v6_64_610.parquet')
    act_df = pd.read_parquet('tw/action/20200611_20200617/')


    act_idfa = all_action(act_df)
    labels = ['dmp_action', 'device_app_info', 'RD', 'campaign_activity', 'model']
    userlists = [rdf1, rdf2, rdf3, rdf4, rdf7]
    for idx, userlist in enumerate(userlists):
        userlists[idx] = userlist[userlist['idfa'].isin(idfa_filter)].reset_index()

    for userlist in userlists:
        userlist['label'] = 0
        userlist.loc[userlist['idfa'].isin(act_idfa), 'label'] = 1
    size_recalls = get_alls(userlists, labels, name='all')

    for recall in size_recalls:
        print(recall[4])
    # print('{}\n'.format(size_recalls[0][4]))
    print('-------')

    act_idfa = all_action_ext(act_df)
    for userlist in userlists:
        userlist['label'] = 0
        userlist.loc[userlist['idfa'].isin(act_idfa), 'label'] = 1
    size_recalls = get_alls(userlists, labels, name='all_ext')
    for recall in size_recalls:
        print(recall[4])
    print('-------')

    act_idfa = all_action_rtb(act_df)
    for userlist in userlists:
        userlist['label'] = 0
        userlist.loc[userlist['idfa'].isin(act_idfa), 'label'] = 1
    size_recalls = get_alls(userlists, labels, name='all_rtb')
    for recall in size_recalls:
        print(recall[4])
    print('-------')

    act_idfa = all_action_rv_rtb(act_df)
    for userlist in userlists:
        userlist['label'] = 0
        userlist.loc[userlist['idfa'].isin(act_idfa), 'label'] = 1
    size_recalls = get_alls(userlists, labels, name='all_rv_rtb')
    for recall in size_recalls:
        print(recall[4])
    print('-------')

    # act_idfa = all_action_inv_RD(act_df)
    # for userlist in userlists:
    #     userlist['label'] = 0
    #     userlist.loc[userlist['idfa'].isin(act_idfa), 'label'] = 1
    # size_recalls = get_alls(userlists, labels, name='rv_RD')
    # for recall in size_recalls:
    #     print(recall[4])
    # print('-------')
    #
    # act_idfa = all_action_inv_others(act_df)
    # for userlist in userlists:
    #     userlist['label'] = 0
    #     userlist.loc[userlist['idfa'].isin(act_idfa), 'label'] = 1
    # size_recalls = get_alls(userlists, labels, name='rv_others')
    # for recall in size_recalls:
    #     print(recall[4])
    # print('-------')


if __name__ == '__main__':
    main_jp()