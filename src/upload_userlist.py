import argparse
import pandas as pd
import os.path
import math

from config import Config
from utils.user_list_utils import UserListServer, save_userlist_csv
from utils.idash_utils import IDashUtils
from utils.logging_utils import logger

LOGGER = logger(__name__)

class UploadUserlist:
    def __init__(self, config, idash_util):
        self.userlist_server = UserListServer(config)
        self.idash_util = idash_util

    def upload_list(self, userlist, listname):
        self.userlist_server.upload_user_list(listname, list(userlist))

    def set_list2cid(self, cids, listname):
        list_name_to_id = self.idash_util.get_list_name_to_id_mapping()
        response = self.idash_util.cids_set_whitelists(cids, [list_name_to_id[listname]])
        LOGGER.info(response)

def parse_args():
    parser = argparse.ArgumentParser(description='update campaign\'s imp bid request user lists')
    parser.add_argument('--config_file', type=str, default='/etc/rtb-app-install-userlist/config.json', help='config json file')
    parser.add_argument('--cids', type=str, nargs='+')
    parser.add_argument('--userlist', type=str)
    parser.add_argument('--name', type=str, required=True)
    parser.add_argument('--size', type=int, default=100000)
    parser.add_argument('--upload', action="store_true")
    parser.add_argument('--group_num', type=int, default=10)
    return parser.parse_args()


def main():
    args = parse_args()
    config = Config(args.config_file)
    idash_util = IDashUtils(config)
    uploader = UploadUserlist(config, idash_util)

    if args.upload:
        df = pd.read_parquet(args.userlist, columns=['idfa'])
        group_size = math.ceil(len(df) / args.group_num)
        for idx in range(args.group_num):
            name = args.name+'/{}/{}'.format(idx+1, args.group_num)
            import pdb; pdb.set_trace()
            tmp = df.iloc[idx*group_size:(idx+1)*group_size, :]
            uploader.upload_list(tmp, name)
        # uploader.upload_list(df['idfa'].values[:args.size], args.name)
    else:
        uploader.set_list2cid(args.cids, args.name)

if __name__ == '__main__':
    main()