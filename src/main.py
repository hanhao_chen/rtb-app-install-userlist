from utils.data_utils import *
import lightgbm as lgb
import re
import pickle as pkl
import _pickle as cpkl
import matplotlib.pylab as plt
from scipy.sparse import vstack
from sklearn.model_selection import train_test_split

def map2idx(row, map):
    return map.get(row['idfa'])

def get_lgb_early_stopping_callback(early_stopping_rounds, skipping_rounds=0):
    """Create a callback that activates LightGBM early stopping.

    LightGBM's built-in early stopping activates prematurely quite often, so we override it with
    this callback (with `skipping_rounds` set to a value > 0).

    Requires at least one validation data and one metric. If more than one is provided to
    `lgb.LGBMClassifier.fit`, will activate early stopping if any of them does not improve.

    Parameters
    ----------
    early_stopping_rounds : int
        Number of iterations with no improvement after which training will be stopped.
    skipping_rounds : int, optional (default=0)
        Number of initial iterations in which this callback will be skipped.

    Returns
    -------
    callback : function
        The callback that can be provided to `LGBMClassifier.fit`.

    Raises
    ------
    ValueError
        If no validation set is provided to LightGBM model.
    """
    # one score per (dataset, metric) combination
    best_score = []
    best_iter = []
    best_score_list = []
    cmp_op = []

    def _format_eval_result(value, show_stdv=True):
        """Format metric string."""
        if len(value) == 4:
            return '%s\'s %s: %g' % (value[0], value[1], value[2])
        elif len(value) == 5:
            if show_stdv:
                return '%s\'s %s: %g + %g' % (value[0], value[1], value[2], value[4])
            else:
                return '%s\'s %s: %g' % (value[0], value[1], value[2])
        else:
            raise ValueError("Wrong metric value")

    def init(env):
        if not env.evaluation_result_list:
            raise ValueError(
                'For early stopping, at least one dataset is required for evaluation')
        for eval_ret in env.evaluation_result_list:
            best_iter.append(0)
            best_score_list.append(None)
            if eval_ret[3]:
                best_score.append(float('-inf'))
                cmp_op.append(operator.gt)
            else:
                best_score.append(float('inf'))
                cmp_op.append(operator.lt)

    def callback(env):
        if not cmp_op:
            init(env)
        if env.iteration < skipping_rounds:
            return
        for i in range(len(env.evaluation_result_list)):
            score = env.evaluation_result_list[i][2]
            if cmp_op[i](score, best_score[i]):
                best_score[i] = score
                best_iter[i] = env.iteration
                best_score_list[i] = env.evaluation_result_list
            elif env.iteration - best_iter[i] >= early_stopping_rounds:
                print('Early stopping, best iteration is:\n[%d]\t%s' % (
                    best_iter[i] + 1, '\t'.join([_format_eval_result(x) for x in best_score_list[i]])))
                raise lgb.callback.EarlyStopException(best_iter[i], best_score_list[i])
            if env.iteration == env.end_iteration - 1:
                print('Did not meet early stopping. Best iteration is:\n[%d]\t%s' % (
                    best_iter[i] + 1, '\t'.join([_format_eval_result(x) for x in best_score_list[i]])))
                raise lgb.callback.EarlyStopException(best_iter[i], best_score_list[i])

    callback.order = 30  # the same order as the built-in early stopping callback
    return callback

def train_model(x_train, y_train, feature_names):
    x_subtrain, x_valid, y_subtrain, y_valid = train_test_split(
        x_train, y_train, stratify=y_train, random_state=1234)
    n_positives = y_train.sum()
    scale_pos_weight = (len(y_train) - n_positives) / n_positives
    lgb_subtrain = lgb.Dataset(x_subtrain, y_subtrain, feature_name=feature_names, categorical_feature=feature_names)
    lgb_valid = lgb.Dataset(x_valid, y_valid, feature_name=feature_names, categorical_feature=feature_names)

    params = {
        'task': 'train',
        'boosting_type': 'gbdt',
        'objective': 'binary',
        'metric': {'auc'},
        'max_depth': 50,
        'num_leaves': 15,
        'learning_rate': 0.08,
        'feature_fraction': 0.9,
        'bagging_fraction': 0.8,
        'bagging_freq': 5,
        'verbose': 0,
        'feature_fraction_seed': 1234,
        'bagging_seed': 1234,
        'scale_pos_weight': scale_pos_weight,#np.log(scale_pos_weight),
        'lambda_l2': 0.1,
        'boost_from_average': "false"
    }

    cbk = get_lgb_early_stopping_callback(early_stopping_rounds=20,
                                          skipping_rounds=10)

    LOGGER.info('Train final model')
    evals_result = {}
    gbm = lgb.train(params,
                    lgb_subtrain,
                    num_boost_round=200,
                    valid_sets=[lgb_subtrain, lgb_valid],
                    valid_names=['train', 'valid'],
                    evals_result=evals_result,
                    feature_name=feature_names,
                    categorical_feature=feature_names,
                    # early_stopping_rounds=50,
                    verbose_eval=5,
                    callbacks=[cbk])
    subtrain_auc = gbm.best_score['train']['auc']
    valid_auc = gbm.best_score['valid']['auc']
    best_iteration = gbm.best_iteration
    LOGGER.info('best iteration %d', best_iteration)

    lgb_train = lgb.Dataset(x_train, y_train)#, feature_name=feature_names, categorical_feature=feature_names)
    gbm = lgb.train(params,
                    lgb_train,
                    num_boost_round=best_iteration,
                    valid_sets=[lgb_train],
                    valid_names=['train'],
                    evals_result=evals_result,
                    feature_name=feature_names,
                    categorical_feature=feature_names,
                    # early_stopping_rounds=50,
                    verbose_eval=5)
    train_auc = evals_result.get('train', {}).get('auc', [])[-1]

    evals = {
        'train': train_auc,
        'subtrain': subtrain_auc,
        'valid': valid_auc,
    }
    return gbm, evals

def train_model_split_install(country, signature, start_date,
                              end_date, _os='ios', osv='10.0', label_file='tw/action/20200513_20200609'):
    """
    tagMap, typeMap = load_token_map(country, signature)
    LOGGER.info('done loading tagMap...')
    filter = load_filter(country, _os, osv, start_date)
    LOGGER.info('done loading filter...')
    train_mat = getMatrix(start_date, end_date, signature, country, 48, tagMap.idx2item)
    LOGGER.info('done loading train_mat...')
    idfaMap = load_idfaMap(country, start_date)
    LOGGER.info('done loading idfaMap...')
    y_df = pd.read_parquet(label_file)
    LOGGER.info('done loading y_df...')

    y_df['idfaIndex'] = y_df.apply(lambda row: map2idx(row, idfaMap.item2idx), axis=1)
    # import pdb; pdb.set_trace()
    y_df = y_df[~pd.isna(y_df['idfaIndex'])]
    y_df['idfaIndex'] = y_df['idfaIndex'].astype(int)
    y_ext = y_df[y_df['is_rtb']==0]
    y_rtb = y_df[y_df['is_rtb']==1]

    df_mat = train_mat[y_df['idfaIndex'].values]
    rtb_mat = train_mat[y_rtb['idfaIndex'].values]
    ext_mat = train_mat[y_ext['idfaIndex'].values]
    pkl.dump((df_mat, rtb_mat, ext_mat, tagMap, typeMap), open('train_model_mat.pkl', 'wb'))
    """
    skiplist = []#['clickActivity/oid_click/TwBO4vGqQI29z-2BshvrQA', 'dmp/flags/a:device_campaign_activity:action:Game:app_install']
    df_mat, rtb_mat, ext_mat, tagMap, typeMap = pkl.load(open('train_model_mat.pkl', 'rb'))
    rtagMap = reduce_matMap(ext_mat, tagMap, typeMap, rtb_mat, skip=skiplist)

    pos_train = gen_reduce_mat(rtb_mat, tagMap, rtagMap)
    neg_train = gen_reduce_mat(ext_mat, tagMap, rtagMap)
    y_train = np.zeros(pos_train.shape[0]+neg_train.shape[0])
    y_train[:pos_train.shape[0]] = 1
    x_train = vstack((pos_train, neg_train))
    # pkl.dump((rtagMap, x_train, y_train), open('train_model.pkl', 'wb'))

    # rtagMap, x_train, y_train = pkl.load(open('train_model.pkl', 'rb'))
    x_train = x_train.astype(dtype=np.float32)

    # import pdb; pdb.set_trace()
    gbm, evals = train_model(x_train, y_train, [re.sub('[^A-Za-z0-9_]+', '', tag) for tag in rtagMap.item2idx.keys()])
    # import pdb; pdb.set_trace()
    plt.figure(figsize=(24, 24))
    lgb.plot_importance(gbm, max_num_features=50)
    plt.title("Feature importance")
    plt.savefig('importance.png')
    importance = gbm.feature_importance(importance_type='gain', iteration=None)
    imp_idx = np.argsort(importance)[::-1]
    imp_list = []
    for idx in imp_idx:
        imp_list.append(rtagMap.idx2item[idx])
    import pdb; pdb.set_trace()


def train_model_entry(country, signature, start_date,
                end_date, _os='ios', osv='10.0', label_file='tw/action/20200604_20200610'):

    """
    tagMap, typeMap = load_token_map(country, signature)
    LOGGER.info('done loading tagMap...')
    filter = load_filter(country, _os, osv, start_date)
    LOGGER.info('done loading filter...')
    train_mat = getMatrix(start_date, end_date, signature, country, 48, tagMap.idx2item)
    LOGGER.info('done loading train_mat...')
    idfaMap = load_idfaMap(country, start_date)
    LOGGER.info('done loading idfaMap...')
    y_df = pd.read_parquet(label_file)
    LOGGER.info('done loading y_df...')
    import pdb; pdb.set_trace()

    y_df['idfaIndex'] = y_df.apply(lambda row: map2idx(row, idfaMap.item2idx), axis=1)
    y_df = y_df[~pd.isna(y_df['idfaIndex'])]
    y_df['idfaIndex'] = y_df['idfaIndex'].astype(int)
    y_ext = y_df[y_df['is_rtb'] == 0]
    y_rtb = y_df[y_df['is_rtb'] == 1]
    rtb_mat = train_mat[y_rtb['idfaIndex'].values]
    ext_mat = train_mat[y_ext['idfaIndex'].values]
    pos_idx = list(y_df['idfaIndex'].values)
    neg_idx = list(set(filter) - set(pos_idx))
    neg_idx = np.random.choice(neg_idx, len(pos_idx)*2000, replace=False)

    pos_mat = train_mat[pos_idx]
    neg_mat = train_mat[neg_idx]
    pkl.dump((rtb_mat, ext_mat, pos_mat, neg_mat, tagMap, typeMap), open('train_mat.pkl', 'wb'))
    """

    rtb_mat, ext_mat, pos_mat, neg_mat, tagMap, typeMap = pkl.load(open('train_mat.pkl', 'rb'))


    rtagMap = reduce_matMap(neg_mat, tagMap, typeMap, pos_mat, skip=['clickActivity/oid_click/TwBO4vGqQI29z-2BshvrQA'])

    pos_train, feats = gen_reduce_mat_v2(rtb_mat, tagMap, rtagMap)
    neg_train, feats = gen_reduce_mat_v2(neg_mat, tagMap, rtagMap)
    y_train = np.zeros(pos_train.shape[0] + neg_train.shape[0])
    y_train[:pos_train.shape[0]] = 1
    x_train = vstack((pos_train, neg_train))
    # pkl.dump((rtagMap, x_train, y_train), open('train_model.pkl', 'wb'))

    # rtagMap, x_train, y_train = pkl.load(open('train_model.pkl', 'rb'))
    x_train = x_train.astype(dtype=np.float32)

    # import pdb; pdb.set_trace()
    gbm, evals = train_model(x_train, y_train, [re.sub('[^A-Za-z0-9_]+', '', tag) for tag in feats])#[re.sub('[^A-Za-z0-9_]+', '', tag) for tag in rtagMap.item2idx.keys()])
    # import pdb; pdb.set_trace()
    plt.figure(figsize=(24, 24))
    lgb.plot_importance(gbm, max_num_features=50)
    plt.title("Feature importance")
    plt.savefig('importance.png')
    importance = gbm.feature_importance(importance_type='gain', iteration=None)
    imp_idx = np.argsort(importance)[::-1]
    imp_list = []
    for idx in imp_idx:
        imp_list.append(feats[idx])

    pkl.dump((gbm, rtagMap), open('gbm.pkl', 'wb'))
    # import pdb;
    # pdb.set_trace()

    gbm, rtagMap = pkl.load(open('gbm.pkl', 'rb'))
    tagMap, typeMap = load_token_map('tw', '7953bfec')
    LOGGER.info('done loading tagMap...')
    filter = load_filter('tw', 'ios', '10.0', '2020-06-04')
    LOGGER.info('done loading filter...')
    train_mat = getMatrix('2020-06-04', '2020-06-10', '7953bfec', 'tw', 48, tagMap.idx2item)
    LOGGER.info('done loading train_mat...')
    idfaMap = load_idfaMap('tw', '2020-06-04')
    LOGGER.info('done loading idfaMap...')

    user_list = model_userlist(train_mat, filter, tagMap, idfaMap, rtagMap, gbm)
    df = pd.DataFrame({'idfa': user_list})
    df.to_parquet('tw/userlist_model_01.parquet')


def train_model_jp_2w(country, signature, start_date,
                   end_date, _os='ios', osv='10.0', label_file='jp/action/20200604_20200610'):

    tagMap, typeMap = load_token_map(country, signature)
    LOGGER.info('done loading tagMap...')
    filter_bid_df = pd.read_parquet('jp/bid/20200604_20200610', engine='pyarrow')
    LOGGER.info('done loading filter...')
    train_mat = getMatrix(start_date, end_date, signature, country, 48, tagMap.idx2item)
    LOGGER.info('done loading train_mat...')
    idfaMap = load_idfaMap(country, start_date)
    LOGGER.info('done loading idfaMap...')

    filter_bid_df_w0 = pd.read_parquet('jp/bid/', engine='pyarrow')
    LOGGER.info('done loading filter...')
    train_mat_w0 = getMatrix('2020-05-21', '2020-05-27', signature, country, 48, tagMap.idx2item)
    LOGGER.info('done loading train_mat...')
    idfaMap_w0 = load_idfaMap(country, '2020-05-21')
    LOGGER.info('done loading idfaMap...')

    y_rt_df = pd.read_parquet('jp/rt_action/20200604_20200610')
    LOGGER.info('done loading y_df...')



    y_rt_df['idfaIndex'] = y_rt_df.apply(lambda row: map2idx(row, idfaMap.item2idx), axis=1)
    filter_bid_df['idfaIndex'] = filter_bid_df.apply(lambda row: map2idx(row, idfaMap.item2idx), axis=1)

    y_rt_df = y_rt_df[~pd.isna(y_rt_df['idfaIndex'])]
    y_rt_df['idfaIndex'] = y_rt_df['idfaIndex'].astype(int)
    filter_bid_df = filter_bid_df[~pd.isna(filter_bid_df['idfaIndex'])]
    filter_bid_df['idfaIndex'] = filter_bid_df['idfaIndex'].astype(int)

    pos_idx = list(set(y_rt_df['idfaIndex'].values) & set(filter_bid_df['idfaIndex'].values))
    all_idx = list(set(filter_bid_df['idfaIndex'].values))
    neg_idx = list(set(all_idx) - set(pos_idx))

    pos_mat = train_mat[pos_idx]
    neg_mat = train_mat[neg_idx]

    cpkl.dump((pos_mat, neg_mat, _pos_mat, _neg_mat, tagMap, typeMap), open('jp/c_train_mat.pkl', 'wb'), protocol=4)
    LOGGER.info('dump training data')


    pos_mat, neg_mat, _pos_mat, _neg_mat, tagMap, typeMap = cpkl.load(open('jp/c_train_mat.pkl', 'rb'))

    LOGGER.info('done loading data')

    rtagMap = reduce_matMap(neg_mat, tagMap, typeMap, pos_mat)

    neg_idx = np.random.choice(neg_mat.shape[0], pos_mat.shape[0] * 1000, replace=False)
    neg_mat = neg_mat[neg_idx]
    neg_mat = vstack((neg_mat, _neg_mat))

    # dump_ranking_v2(tagMap, pos_mat)

    pos_train, feats, usedidx = gen_reduce_mat_v2(pos_mat, tagMap, rtagMap)
    neg_train, feats, usedidx = gen_reduce_mat_v2(neg_mat, tagMap, rtagMap)
    LOGGER.info('done reduce mat_v2')
    # pos_train, feats = gen_reduce_mat_v3(pos_train, tagMap, rtagMap, usedidx)
    # neg_train, feats = gen_reduce_mat_v3(neg_train, tagMap, rtagMap, usedidx)

    LOGGER.info('mat shap (%d, %d)', pos_train.shape[0], pos_train.shape[1])
    LOGGER.info('mat shap (%d, %d)', neg_train.shape[0], neg_train.shape[1])

    y_train = np.zeros(pos_train.shape[0] + neg_train.shape[0])
    y_train[:pos_train.shape[0]] = 1
    x_train = vstack((pos_train, neg_train))
    LOGGER.info('pos size: %d, neg size: %d', pos_train.shape[0], neg_train.shape[0])
    # pkl.dump((rtagMap, x_train, y_train), open('train_model.pkl', 'wb'))

    # rtagMap, x_train, y_train = pkl.load(open('train_model.pkl', 'rb'))
    x_train = x_train.astype(dtype=np.float32)

    # import pdb; pdb.set_trace()
    gbm, evals = train_model(x_train, y_train, [re.sub('[^A-Za-z0-9_]+', '', tag) for tag in
                                                feats])  # [re.sub('[^A-Za-z0-9_]+', '', tag) for tag in rtagMap.item2idx.keys()])
    # import pdb; pdb.set_trace()
    plt.figure(figsize=(24, 24))
    lgb.plot_importance(gbm, max_num_features=50)
    plt.title("Feature importance")
    plt.savefig('importance.png')
    importance = gbm.feature_importance(importance_type='gain', iteration=None)
    imp_idx = np.argsort(importance)[::-1]
    imp_list = []
    for idx in imp_idx:
        imp_list.append(feats[idx])

    pkl.dump((gbm, rtagMap), open('gbm.pkl', 'wb'))
    import pdb;
    pdb.set_trace()

    gbm, rtagMap = pkl.load(open('gbm.pkl', 'rb'))
    tagMap, typeMap = load_token_map(country, signature)
    LOGGER.info('done loading tagMap...')
    filter = load_filter('jp', 'ios', '10.0', '2020-05-28')
    LOGGER.info('done loading filter...')
    train_mat = getMatrix('2020-05-28', '2020-06-03', signature, 'jp', 48, tagMap.idx2item)
    LOGGER.info('done loading train_mat...')
    idfaMap = load_idfaMap(country, '2020-05-28')
    LOGGER.info('done loading idfaMap...')

    user_list = model_userlist(train_mat, filter, tagMap, idfaMap, rtagMap, gbm)
    df = pd.DataFrame({'idfa': user_list})
    df.to_parquet('jp/userlist_model_w1_showneg.parquet')
    del train_mat, idfaMap, filter

    tagMap, typeMap = load_token_map(country, signature)
    LOGGER.info('done loading tagMap...')
    filter = load_filter('jp', 'ios', '10.0', '2020-06-04')
    LOGGER.info('done loading filter...')
    train_mat = getMatrix('2020-06-04', '2020-06-10', signature, 'jp', 48, tagMap.idx2item)
    LOGGER.info('done loading train_mat...')
    idfaMap = load_idfaMap(country, '2020-06-04')
    LOGGER.info('done loading idfaMap...')

    user_list = model_userlist(train_mat, filter, tagMap, idfaMap, rtagMap, gbm)
    df = pd.DataFrame({'idfa': user_list})
    df.to_parquet('jp/userlist_model_w2_showneg.parquet')

    # user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_comic)
    # df = pd.DataFrame({'idfa': user_list})
    # df.to_parquet('jp/userlist_posTag_comic.parquet')
    #
    # user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_genre)
    # df = pd.DataFrame({'idfa': user_list})
    # df.to_parquet('jp/userlist_posTag_genre.parquet')


def train_model_jp(country, signature, start_date,
                end_date, _os='ios', osv='10.0', label_file='jp/action/20200604_20200610'):
    """
    tagMap, typeMap = load_token_map(country, signature)
    LOGGER.info('done loading tagMap...')
    # filter = load_filter(country, _os, osv, start_date)
    # idfa_filter = load_filter_idfa('', 'ios', '10.0', '', dir='jp/bid/20200604_20200610')
    filter_df = pd.read_parquet('jp/show/20200604_20200610', engine='pyarrow')
    filter_bid_df = pd.read_parquet('jp/bid/20200604_20200610', engine='pyarrow')
    LOGGER.info('done loading filter...')
    train_mat = getMatrix(start_date, end_date, signature, country, 48, tagMap.idx2item)
    LOGGER.info('done loading train_mat...')
    idfaMap = load_idfaMap(country, start_date)
    LOGGER.info('done loading idfaMap...')
    y_df = pd.read_parquet(label_file)
    y_rt_df = pd.read_parquet('jp/rt_action/20200604_20200610')
    LOGGER.info('done loading y_df...')

    y_df['idfaIndex'] = y_df.apply(lambda row: map2idx(row, idfaMap.item2idx), axis=1)
    filter_df['idfaIndex'] = filter_df.apply(lambda row: map2idx(row, idfaMap.item2idx), axis=1)

    y_df = y_df[~pd.isna(y_df['idfaIndex'])]
    y_df['idfaIndex'] = y_df['idfaIndex'].astype(int)
    filter_df = filter_df[~pd.isna(filter_df['idfaIndex'])]
    filter_df['idfaIndex'] = filter_df['idfaIndex'].astype(int)

    _pos_idx = list(set(y_df[y_df['vtcv']>0]['idfaIndex'].values))
    show_idx = list(set(filter_df['idfaIndex'].values))
    _neg_idx = list(set(show_idx) - set(_pos_idx))

    _pos_mat = train_mat[_pos_idx]
    _neg_mat = train_mat[_neg_idx]

    y_rt_df['idfaIndex'] = y_rt_df.apply(lambda row: map2idx(row, idfaMap.item2idx), axis=1)
    filter_bid_df['idfaIndex'] = filter_bid_df.apply(lambda row: map2idx(row, idfaMap.item2idx), axis=1)

    y_rt_df = y_rt_df[~pd.isna(y_rt_df['idfaIndex'])]
    y_rt_df['idfaIndex'] = y_rt_df['idfaIndex'].astype(int)
    filter_bid_df = filter_bid_df[~pd.isna(filter_bid_df['idfaIndex'])]
    filter_bid_df['idfaIndex'] = filter_bid_df['idfaIndex'].astype(int)

    pos_idx = list(set(y_rt_df['idfaIndex'].values) & set(filter_bid_df['idfaIndex'].values))
    all_idx = list(set(filter_bid_df['idfaIndex'].values))
    neg_idx = list(set(all_idx) - set(pos_idx))

    pos_mat = train_mat[pos_idx]
    neg_mat = train_mat[neg_idx]

    cpkl.dump((pos_mat, neg_mat, _pos_mat, _neg_mat, tagMap, typeMap), open('jp/c_train_mat.pkl', 'wb'), protocol=4)
    LOGGER.info('dump training data')
    """

    pos_mat, neg_mat, _pos_mat, _neg_mat, tagMap, typeMap = cpkl.load(open('jp/c_train_mat.pkl', 'rb'))

    LOGGER.info('done loading data')

    rtagMap = reduce_matMap(neg_mat, tagMap, typeMap, pos_mat)

    neg_idx = np.random.choice(neg_mat.shape[0], pos_mat.shape[0] * 1000, replace=False)
    neg_mat = neg_mat[neg_idx]
    neg_mat = vstack((neg_mat, _neg_mat))

    # dump_ranking_v2(tagMap, pos_mat)
    
    pos_train, feats, usedidx = gen_reduce_mat_v2(pos_mat, tagMap, rtagMap)
    neg_train, feats, usedidx = gen_reduce_mat_v2(neg_mat, tagMap, rtagMap)
    LOGGER.info('done reduce mat_v2')
    # pos_train, feats = gen_reduce_mat_v3(pos_train, tagMap, rtagMap, usedidx)
    # neg_train, feats = gen_reduce_mat_v3(neg_train, tagMap, rtagMap, usedidx)

    LOGGER.info('mat shap (%d, %d)', pos_train.shape[0], pos_train.shape[1])
    LOGGER.info('mat shap (%d, %d)', neg_train.shape[0], neg_train.shape[1])

    y_train = np.zeros(pos_train.shape[0] + neg_train.shape[0])
    y_train[:pos_train.shape[0]] = 1
    x_train = vstack((pos_train, neg_train))
    LOGGER.info('pos size: %d, neg size: %d', pos_train.shape[0], neg_train.shape[0])
    # pkl.dump((rtagMap, x_train, y_train), open('train_model.pkl', 'wb'))

    # rtagMap, x_train, y_train = pkl.load(open('train_model.pkl', 'rb'))
    x_train = x_train.astype(dtype=np.float32)

    # import pdb; pdb.set_trace()
    gbm, evals = train_model(x_train, y_train, [re.sub('[^A-Za-z0-9_]+', '', tag) for tag in
                                                feats])  # [re.sub('[^A-Za-z0-9_]+', '', tag) for tag in rtagMap.item2idx.keys()])
    # import pdb; pdb.set_trace()
    plt.figure(figsize=(24, 24))
    lgb.plot_importance(gbm, max_num_features=50)
    plt.title("Feature importance")
    plt.savefig('importance.png')
    importance = gbm.feature_importance(importance_type='gain', iteration=None)
    imp_idx = np.argsort(importance)[::-1]
    imp_list = []
    for idx in imp_idx:
        imp_list.append(feats[idx])

    pkl.dump((gbm, rtagMap), open('gbm.pkl', 'wb'))
    import pdb;
    pdb.set_trace()

    gbm, rtagMap = pkl.load(open('gbm.pkl', 'rb'))
    tagMap, typeMap = load_token_map(country, signature)
    LOGGER.info('done loading tagMap...')
    filter = load_filter('jp', 'ios', '10.0', '2020-05-28')
    LOGGER.info('done loading filter...')
    train_mat = getMatrix('2020-05-28', '2020-06-03', signature, 'jp', 48, tagMap.idx2item)
    LOGGER.info('done loading train_mat...')
    idfaMap = load_idfaMap(country, '2020-05-28')
    LOGGER.info('done loading idfaMap...')

    user_list = model_userlist(train_mat, filter, tagMap, idfaMap, rtagMap, gbm)
    df = pd.DataFrame({'idfa': user_list})
    df.to_parquet('jp/userlist_model_w1_showneg.parquet')
    del train_mat, idfaMap, filter

    tagMap, typeMap = load_token_map(country, signature)
    LOGGER.info('done loading tagMap...')
    filter = load_filter('jp', 'ios', '10.0', '2020-06-04')
    LOGGER.info('done loading filter...')
    train_mat = getMatrix('2020-06-04', '2020-06-10', signature, 'jp', 48, tagMap.idx2item)
    LOGGER.info('done loading train_mat...')
    idfaMap = load_idfaMap(country, '2020-06-04')
    LOGGER.info('done loading idfaMap...')

    user_list = model_userlist(train_mat, filter, tagMap, idfaMap, rtagMap, gbm)
    df = pd.DataFrame({'idfa': user_list})
    df.to_parquet('jp/userlist_model_w2_showneg.parquet')

    # user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_comic)
    # df = pd.DataFrame({'idfa': user_list})
    # df.to_parquet('jp/userlist_posTag_comic.parquet')
    #
    # user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_genre)
    # df = pd.DataFrame({'idfa': user_list})
    # df.to_parquet('jp/userlist_posTag_genre.parquet')

def main():
    # tagMap, typeMap = load_token_map('tw', 'f56afc08')
    # filter = load_filter('tw', 'ios', '10.0', '2020-06-09')
    # train_mat = getMatrix('2020-06-09', '2020-07-06', 'f56afc08', 'tw', 48, tagMap.idx2item)
    # y_train_t1 = load_target_oid('tw', 'TwBO4vGqQI29z-2BshvrQA', '2020-06-09', '2020-07-06', target=1, force=False)
    # y_train_t2 = load_target_oid('tw', 'TwBO4vGqQI29z-2BshvrQA', '2020-06-09', '2020-07-06', target=2, force=False)
    # dump_ranking(tagMap, train_mat, filter, y_train_t1, y_train_t2, feat='dmp')
    # import pdb; pdb.set_trace()

    tagMap, typeMap = load_token_map('kr', '38b4bd84')
    LOGGER.info('done loading tagMap...')
    filter = load_filter('kr', 'ios', '10.0', '2020-06-12')
    LOGGER.info('done loading filter...')
    train_mat = getMatrix('2020-06-12', '2020-07-09', '38b4bd84', 'kr', 48, tagMap.idx2item)
    LOGGER.info('done loading train_mat...')
    idfaMap = load_idfaMap('kr', '2020-06-12')
    # import pdb;pdb.set_trace()
    LOGGER.info('done loading idfaMap...')

    user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_kaka)
    df = pd.DataFrame({'idfa': user_list})
    df.to_parquet('kr/userlist_ios.parquet')

    filter = load_filter('kr', 'android', '2.0', '2020-06-12')
    LOGGER.info('done loading filter...')

    user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_kaka)
    df = pd.DataFrame({'idfa': user_list})
    df.to_parquet('kr/userlist_and.parquet')

    """
    user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_v2)
    df = pd.DataFrame({'idfa': user_list})
    df.to_parquet('tw/userlist_posTag_v2.parquet')

    user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_v3)
    df = pd.DataFrame({'idfa': user_list})
    df.to_parquet('tw/userlist_posTag_v3.parquet')

    user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_v4)
    df = pd.DataFrame({'idfa': user_list})
    df.to_parquet('tw/userlist_posTag_v4.parquet')
    """


    # tagMap, typeMap = load_token_map('tw', 'fdec4090')
    # LOGGER.info('done loading tagMap...')
    # filter = load_filter('tw', 'ios', '10.0', '2020-06-04')
    # LOGGER.info('done loading filter...')
    # train_mat = getMatrix('2020-06-04', '2020-06-10', 'fdec4090', 'tw', 48, tagMap.idx2item)
    # LOGGER.info('done loading train_mat...')
    # idfaMap = load_idfaMap('tw', '2020-06-04')
    # # import pdb;pdb.set_trace()
    # LOGGER.info('done loading idfaMap...')

    # user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_v1)
    # df = pd.DataFrame({'idfa': user_list})
    # df.to_parquet('tw/userlist_posTag_v1_64_610.parquet')
    #
    # user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_v2)
    # df = pd.DataFrame({'idfa': user_list})
    # df.to_parquet('tw/userlist_posTag_v2_64_610.parquet')
    #
    # user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_v3)
    # df = pd.DataFrame({'idfa': user_list})
    # df.to_parquet('tw/userlist_posTag_v3_64_610.parquet')
    #
    # user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_v4)
    # df = pd.DataFrame({'idfa': user_list})
    # df.to_parquet('tw/userlist_posTag_v4_64_610.parquet')

    # user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_v5)
    # df = pd.DataFrame({'idfa': user_list})
    # df.to_parquet('tw/userlist_posTag_v5_64_610.parquet')
    #
    # user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_v6)
    # df = pd.DataFrame({'idfa': user_list})
    # df.to_parquet('tw/userlist_posTag_v6_64_610.parquet')

    # user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_v7)
    # df = pd.DataFrame({'idfa': user_list})
    # df.to_parquet('tw/userlist_posTag_v7_64_610.parquet')



    """
    tagMap, typeMap = load_token_map('tw', 'bc7289ca')
    LOGGER.info('done loading tagMap...')
    filter = load_filter('tw', 'ios', '10.0', '2020-05-13')
    LOGGER.info('done loading filter...')
    train_mat = getMatrix('2020-05-13', '2020-06-09', 'bc7289ca', 'tw', 48, tagMap.idx2item)
    LOGGER.info('done loading train_mat...')
    idfaMap = load_idfaMap('tw', '2020-05-13')
    LOGGER.info('done loading idfaMap...')
    user_list = rule_userlist(train_mat, filter, tagMap, idfaMap, c.posTag_v2)
    df = pd.DataFrame({'idfa': user_list})
    df.to_parquet('tw/userlist_device_app_info.parquet')

    score_df = rule_userlist_score_df(train_mat, filter, tagMap, idfaMap, c.posTag_v2)
    score_df.to_parquet('tw/userlist_device_app_info_score.parquet')
    """
    # train_model_split_install('tw', 'bc7289ca', '2020-05-13', '2020-06-09')

    # train_model_entry('tw', '7953bfec', '2020-05-28', '2020-06-03')
    train_model_jp('jp', 'e88d7c2d', '2020-05-28', '2020-06-03')

if __name__ == '__main__':
    main()
