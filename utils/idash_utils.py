import json
import time

from utils.request_utils import RequestUtils


class IDashUtils(RequestUtils):

    def __init__(self, config, expired_time=3600):
        super().__init__(config, expired_time=expired_time)
        self.host, self.username, self.password = config.get_idash_credentials()

    def login(self):
        timestamp = int(time.time())
        if not self.cookies or timestamp - self.cookies_timestamp >= self.expired_time:
            payload = {'_': timestamp, 'username': self.username, 'password': self.password}
            response = self.send_post_request(
                self.host + '/login', data=payload, headers={'Connection': 'close'}, is_login_request=True)
            self.cookies = response.cookies
            self.cookies_timestamp = timestamp

    def read_oid_performance(self, oids, start_ts, end_ts, group, timeout=180):
        url = self.host + '/performance?order_ids={}&customer_view=cm_view'.format(json.dumps(oids))
        for key, val in [('start_ts', start_ts), ('end_ts', end_ts), ('group', group)]:
            url += '&{}={}'.format(key, val)
        self.logger.info(url)
        return self.send_get_request(url, timeout=timeout).json()

    def read_userlist_job_ids(self, job_ids):
        query = {'_id': {'$in': list(job_ids)}}
        url = self.host + '/retargeting_dashboard/query/job_queue?query={}'.format(json.dumps(query))
        self.logger.info(url)
        return self.send_get_request(url).json()

    def cids_set_blacklists(self, cids, list_ids):
        url = self.host + '/update/retargeting_list/campaign_setting'
        params = {'cids': list(cids), 'lists_status': {}}
        for list_id in list_ids:
            params['lists_status'][list_id] = {'status': 'cap_on'}
        return self.send_post_request(url, json=params).json()

    def cids_set_whitelists(self, cids, list_ids):
        url = self.host + '/update/retargeting_list/campaign_setting'
        params = {'cids': list(cids), 'lists_status': {}}
        for list_id in list_ids:
            params['lists_status'][list_id] = {'status': 'on'}
        return self.send_post_request(url, json=params).json()

    def get_oid_country(self, oid):
        url = self.host + '/read/campaign?oid={}'.format(oid)
        self.logger.info(url)
        reply = self.send_get_request(url).json()[0]
        return reply['require_regions']

    def get_list_name_to_id_mapping(self):
        url = self.host + '/read/retargeting_share_lists?list_destination=bidder_bloomfilter&fields={}'.format(
            json.dumps(['list_id', 'name']))
        response = self.send_get_request(url).json()
        return {log['name']: log['list_id'] for log in response['data']}
