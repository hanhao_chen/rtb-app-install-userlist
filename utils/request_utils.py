import requests
import time
from functools import wraps


def request_exception_handler(max_retry):
    def handler(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            # args[0]: self
            # args[1]: url
            response = None
            for retry in range(max_retry):
                try:
                    response = func(*args, **kwargs)
                except requests.exceptions.Timeout:
                    args[0].logger.exception('Timeout of %s, count: %d', args[1], retry + 1)
                    continue
                except requests.exceptions.ConnectionError:
                    args[0].logger.exception('Connection error of %s, count: %d', args[1], retry + 1)
                    continue
                except requests.exceptions.RequestException:
                    args[0].logger.exception('Request exception of %s, count: %d', args[1], retry + 1)
                    continue
                if response.status_code == 200:
                    return response
                args[0].logger.warning('request to %s, status %s', args[1], response.status_code)
                if response.status_code in range(400, 500) or retry == max_retry - 1:
                    response.raise_for_status()
                time.sleep(0.5)
            return response
        return wrapper
    return handler


class RequestUtils:

    def __init__(self, config, expired_time=3600):
        self.logger = config.get_logger(__name__)
        self.expired_time = expired_time
        self.cookies = None
        self.cookies_timestamp = None

    def login(self):
        pass

    @request_exception_handler(5)
    def send_post_request(self, url, data=None, json=None, headers=None, timeout=30, files=None,
                          is_login_request=False):
        if not is_login_request:
            self.login()
        return requests.post(
            url, data=data, json=json, headers=headers, cookies=self.cookies, timeout=timeout, files=files)

    @request_exception_handler(5)
    def send_get_request(self, url, params=None, headers=None, timeout=30):
        self.login()
        return requests.get(url, params=params, headers=headers, cookies=self.cookies, timeout=timeout)
