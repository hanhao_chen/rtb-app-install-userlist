import ctypes
from datetime import datetime
import pyarrow
import pyarrow.parquet as pq
import os, sys
import concurrent.futures
from scipy.sparse import lil_matrix
import queue
import numpy as np
import mmap
from copy import copy
from scipy.sparse import csr_matrix
import h5py
import pandas as pd
import os
import operator
import functools
import pickle as pkl
import _pickle as cpkl
import mmap
import numpy as np
from collections import defaultdict, Counter
import constants as c
from utils.logging_utils import logger

LOGGER = logger(__name__)

class mapping:
    def __init__(self, data):
        self.item2idx = {}
        self.idx2item = {}
        if isinstance(data, tuple):
            self.build_dict_tuple(data)
        else:
            self.build_dict(data)

    def build_dict_tuple(self, data):
        for idx, item in enumerate(data[0]):
            self.item2idx[item] = data[1][idx]
            self.idx2item[data[1][idx]] = item

    def build_dict(self, data):
        for idx, item in enumerate(data):
            self.item2idx[item] = idx
            self.idx2item[idx] = item


def functools_reduce_iconcat(a):
    return functools.reduce(operator.iconcat, a, [])


def generate_new_feat(df, tagMap, newTagMap):
    pass


def load_all_data(country, signature, start_date, end_date, targetIDFA, site_id,
                  target=1, neg_rate=1000, force=False):
    tagMap, typeMap = load_token_map(country, signature)
    raw_df = load_idfa_feature(country, signature, start_date, end_date, targetIDFA, force=force)
    y_IDFA = load_target(country, site_id, start_date, end_date, target, force=force)
    newTagMap = get_used_tag(raw_df, tagMap, typeMap, force=force)

    raw_df['label'] = 0
    raw_df.loc[raw_df['idfaIndex'].isin(y_IDFA), 'label'] = 1

    pos_df = raw_df[raw_df['label'] > 0]
    neg_df = raw_df[raw_df['label'] == 0]
    sample_neg_size = min(neg_rate * len(pos_df), len(neg_df))
    neg_df = neg_df.sample(sample_neg_size, random_state=1234)
    sub_raw_df = pd.concat([pos_df, neg_df], sort=False)


def gen_reduce_mat(mat, tagMap, rtagMap):
    rmat = lil_matrix((mat.shape[0], len(rtagMap.item2idx)), dtype=np.int8)
    LOGGER.info('creating mat done shape %d, %d', mat.shape[0], len(rtagMap.item2idx))
    for cidx, (tag, idx) in enumerate(tagMap.item2idx.items()):
        print(cidx)
        try:
            weight = int(tag.split('=')[-1])
        except:
            weight = 1
        tag_v2 = tag.split('=')[0]
        ridx = rtagMap.item2idx.get(tag_v2)
        if ridx is not None:
            rmat[:, ridx] += mat[:, idx] * weight
    return rmat


def gen_reduce_mat_v3(mat, tagMap, rtagMap, usedidx):
    mat = np.array(mat.todense())
    rmat = np.zeros((mat.shape[0], len(rtagMap.item2idx)))
    # num_feats = []
    idxMap = defaultdict(list)
    for midx, uidx in enumerate(usedidx):
        sys.stdout.write("\r {}/{}...".format(midx, len(usedidx)))
        tag = tagMap.idx2item[uidx]
        weight = 1
        if 'device_location/city_major' not in tag:
            tag_v2 = tag.split('=')[0]
            try:
                weight = int(tag.split('=')[1])
            except:
                weight = 1
        else:
            tag_v2 = tag
        idx = rtagMap.item2idx[tag_v2]
        idxMap[idx].append(midx)
        rmat[:, idx] += mat[:, midx] * weight
    # import pdb;pdb.set_trace()


    feats = [rtagMap.idx2item[idx] for idx in range(len(rtagMap.item2idx))]
    return rmat, feats

def gen_reduce_mat_v2(mat, tagMap, rtagMap):

    usedidx = []
    feats = []
    for cidx, (tag, idx) in enumerate(tagMap.item2idx.items()):
        try:
            weight = int(tag.split('=')[-1])
        except:
            weight = 1
        if 'device_location/city_major' not in tag:
            tag_v2 = tag.split('=')[0]
        else:
            tag_v2 = tag
        ridx = rtagMap.item2idx.get(tag_v2)
        if ridx is not None:
            usedidx.append(idx)
    usedidx = sorted(usedidx)
    for idx in usedidx:
        feats.append(tagMap.idx2item[idx])
    return mat[:, usedidx], feats, usedidx


def reduce_matMap(train_mat, tagMap, typeMap, y_mat, skip=[]):
    sum_mat = train_mat.sum(axis=0)

    typeDict_v2 = {}
    for type in typeMap.item2idx.keys():
        typeDict_v2[type] = defaultdict(int)

    for tag, idx in tagMap.item2idx.items():
        try:
            weight = 1#int(tag.split('=')[-1])
        except:
            weight = 1
        type = tag.split('/')[0]
        if 'device_location/city_major' not in tag:
            tag_v2 = tag.split('=')[0]
        else:
            tag_v2 = tag
        typeDict_v2[type][tag_v2] += sum_mat[0, idx] * weight

    usedTag_all = []
    for type, tagDict in typeDict_v2.items():
        total = np.array(list(tagDict.values())).sum()
        curr = 0
        count = 0
        for tag_v2, value in sorted(tagDict.items(), key=lambda x:x[1], reverse=True):
            if count < 10:
                print(tag_v2, ' : ', value)
            count += 1
            usedTag_all.append(tag_v2)
            curr += value
            if curr/total >= 0.8:
                break
    print('-------------------')
    sum_mat = y_mat.sum(axis=0)
    typeDict_v2 = {}
    for type in typeMap.item2idx.keys():
        typeDict_v2[type] = defaultdict(int)

    for tag, idx in tagMap.item2idx.items():
        try:
            weight = 1#int(tag.split('=')[-1])
        except:
            weight = 1
        type = tag.split('/')[0]
        if 'device_location/city_major' not in tag:
            tag_v2 = tag.split('=')[0]
        else:
            tag_v2 = tag
        typeDict_v2[type][tag_v2] += sum_mat[0, idx] * weight

    usedTag_y = []
    for type, tagDict in typeDict_v2.items():
        total = np.array(list(tagDict.values())).sum()
        curr = 0
        count = 0
        for tag_v2, value in sorted(tagDict.items(), key=lambda x: x[1], reverse=True):
            if count < 10:
                print(tag_v2, ' : ', value)
            count += 1
            usedTag_y.append(tag_v2)
            curr += value
            if curr / total >= 0.8:
                break

    usedTag = list((set(usedTag_y) | set(usedTag_all)) - set(skip))
    # usedTag = [tag for tag in usedTag if '1462877149' not in usedTag]
    LOGGER.info('all feat: %d, y feat: %d', len(usedTag_all), len(usedTag_y))
    LOGGER.info('final feat: %d', len(usedTag))

    rtagMap = mapping(usedTag)
    return rtagMap

def get_used_tag(train_mat, tagMap, typeMap):
    vecs = list(df['vector'].values)
    vecs = functools_reduce_iconcat(vecs)
    countDict = Counter(vecs)

    typeDict = {}
    typeDict_v2 = {}
    for type in typeMap.item2idx.keys():
        typeDict[type] = dict()
        typeDict_v2[type] = defaultdict(int)

    for tagidx, count in countDict.items():
        tag = tagMap.idx2item[tagidx]
        type = tag.split('/')[0]
        typeDict[type][tag] = count

    for type, tagDict in typeDict.items():
        for tag, count in tagDict.items():
            _tag = tag.split('=')[0]
            typeDict_v2[type][_tag] += count

    used_tag = []
    for type, tagDict in typeDict_v2.items():
        tag_total = tagDict.values().sum()
        curr_count = 0
        for _tag, count in sorted(tagDict.items(), key=lambda x:x[1], reverse=True):
            if curr_count / tag_total > 0.9:
                break
            curr_count += count
            used_tag.append(_tag)

    newTagMap = mapping(used_tag)
    return newTagMap


def load_target_oid(country, oid, start_date, end_date, target=1, force=False):
    tmp_path = "{}/target-{}-oid={}-StartUTCDate={}-EndUTCDate={}.pkl".format(country, target, oid, start_date, end_date)
    if os.path.exists(tmp_path) and not force:
        return pkl.load(open(tmp_path, 'rb'))
    path = os.path.join(country, "rt3", "oid=%s" % oid, "target=%d" % target, "StartUTCDate=%s" % start_date,
                        'EndUTCDate=%s' % end_date)
    df = pd.read_parquet(path, engine='pyarrow')
    y_IDFA = set(df['idfaIndex'].values)
    pkl.dump(y_IDFA, open(tmp_path, 'wb'))
    return y_IDFA

def load_target(country, site_id, start_date, end_date, target=1, force=False):
    tmp_path = "{}/target-{}-site_id={}-StartUTCDate={}-EndUTCDate={}.pkl".format(country, target, site_id, start_date, end_date)
    if os.path.exists(tmp_path) and not force:
        return pkl.load(open(tmp_path, 'rb'))
    path = os.path.join(country, "rt3", "site_id=%s"%site_id, "target=%d"%target, "StartUTCDate=%s" % start_date, 'EndUTCDate=%s' % end_date)
    df = pd.read_parquet(path, engine='pyarrow')
    y_IDFA = set(df['idfaIndex'].values)
    pkl.dump(y_IDFA, open(tmp_path, 'wb'))
    return y_IDFA


def dimVectorParquet(path):
    pool = pyarrow.proxy_memory_pool(pyarrow.default_memory_pool())
    def createScope():
        tb = pq.read_table(path, use_threads = False, memory_map = True)
        pd = tb.to_pandas(use_threads = False, memory_pool = pool)
        indices_size = 0
        arrs = pd.iloc[:,1].values
        for arr in arrs:
            indices_size += len(arr)
        return indices_size
    return createScope()


def getIDFAIndexRange(start_date, end_date, root):
    path_pattern = "tmp/IDFA/StartUTCDate={}/EndUTCDate={}"
    root = os.path.join(root, path_pattern.format(start_date, end_date))
    paths = [os.path.join(root, fname) for fname in os.listdir(root) if "parquet" in fname]
    return (0, np.sum([pq.read_metadata(path).num_rows for path in paths]))


def parseVector(index, path, start, end):
    pool = pyarrow.proxy_memory_pool(pyarrow.default_memory_pool())
    def createScope(index, path, start, end):
        tb = pq.read_table(path, use_threads = False, memory_map = True)
        pd = tb.to_pandas(use_threads = False, memory_pool = pool)
        row_indexes = pd.iloc[:,0].values
        arrs = pd.iloc[:,1].values
        local_indptr = np.frombuffer(indptr_buffer, ctypes.c_int64)
        local_indices = np.frombuffer(indices_buffer, ctypes.c_int64)
        for row_index, arr in zip(row_indexes, arrs):
            assert(start + len(arr) <= end)
            local_indices[range(start, start + len(arr))] = arr
            local_indptr[row_index + 1] = start + len(arr)
            start += len(arr)
        assert(start == end)
        return index
    try:
        return createScope(index, path, start, end)
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

def getMatrix(start_date, end_date, signature, root, max_workers, token_map, force=False):
    tmp_path = "{}/vector-{}-StartUTCDate={}-EndUTCDate={}.pkl".format(root, signature, start_date, end_date)
    if os.path.exists(tmp_path) and not force:
        return cpkl.load(open(tmp_path, 'rb'))
    global indptr_buffer
    global indices_buffer
    token_index_arr = np.array(list(token_map.keys()))
    token_index_arr.sort()
    assert(np.min(token_index_arr) == 0)
    assert(np.all(np.diff(token_index_arr) == 1))
    ncol = len(token_index_arr)
    path_pattern = "vector/{}/StartUTCDate={}/EndUTCDate={}"
    vector_root = os.path.join(root, path_pattern.format(signature, start_date, end_date))
    paths = [os.path.join(vector_root, fname) for fname in os.listdir(vector_root) if "parquet" in fname]
    paths.sort()
    # get dimension for memroy allocation
    q = queue.Queue()
    submit_index = 0
    sys.stdout.write("{} Calculating nrow...\n".format(datetime.now()))
    nrow = getIDFAIndexRange(start_date, end_date, root)[1]
    indices_sizes = [0]
    sys.stdout.write("{} Calculating indices_sizes...\n".format(datetime.now()))
    with concurrent.futures.ProcessPoolExecutor(max_workers = int(max_workers)) as executor:
        while submit_index < len(paths) and submit_index < 2 * int(max_workers):
            q.put(executor.submit(dimVectorParquet, paths[submit_index]))
            submit_index += 1
        while not q.empty():
            local_indices_size = q.get().result()
            indices_sizes.append(local_indices_size)
            sys.stdout.write("\r {}/{}...".format(len(indices_sizes), len(paths)))
            if submit_index < len(paths):
                q.put(executor.submit(dimVectorParquet, paths[submit_index]))
                submit_index += 1
    indices_sizes = np.array(indices_sizes)
    indices_size = np.sum(indices_sizes)
    indices_sizes = list(np.cumsum(indices_sizes))
    sys.stdout.write("\r{} Allocating indptr...\n".format(datetime.now()))
    with mmap.mmap(-1, (nrow + 1) * ctypes.sizeof(ctypes.c_int64)) as indptr_buffer:
        indptr = np.frombuffer(indptr_buffer, ctypes.c_int64)
        indptr[:] = -1
        sys.stdout.write("{} Allocating indices...\n".format(datetime.now()))
        with mmap.mmap(-1, indices_size * ctypes.sizeof(ctypes.c_int64)) as indices_buffer:
            indices = np.frombuffer(indices_buffer, ctypes.c_int64)
            indices[:] = -1
            last_row_index = -1
            del q
            del submit_index
            pc_q = queue.Queue()
            indptr[0] = 0
            arr_index = 0
            sys.stdout.write("{} Constructing indptr and indices...\n".format(datetime.now()))
            assert(len(paths) + 1== len(indices_sizes))
            with concurrent.futures.ProcessPoolExecutor(max_workers = int(max_workers)) as pc_executor:
                futures = [
                    pc_executor.submit(
                        parseVector,
                        index = pc_submit_index,
                        path = paths[pc_submit_index],
                        start = indices_sizes[pc_submit_index],
                        end = indices_sizes[pc_submit_index + 1]
                    )
                    for pc_submit_index in range(len(paths))
                ]
                for progress_index in range(len(futures)):
                    future = futures[progress_index]
                    assert(future.result() == progress_index)
                    sys.stdout.write("\r {}/{}...".format(progress_index + 1, len(paths)))
            idx = np.where(indptr == -1)[0]
            idx.sort()
            for i in idx:
                indptr[i] = indptr[i-1]

            assert(np.all(indices != -1))
            assert(np.all(indptr != -1))
            assert(np.all(np.diff(indptr) >= 0))
            sys.stdout.write("\r {} Converting to DMatrix!\n".format(datetime.now()))
            from scipy.sparse import csr_matrix
            data = np.ones(len(indices), dtype = np.float32)
            result = csr_matrix((data, indices, indptr), shape = (nrow, ncol), copy = True)
            del indices
        indices_buffer = None
        del indptr
    indptr_buffer = None
    import gc
    gc.collect()
    cpkl.dump(result, open(tmp_path, 'wb'), protocol = 4)
    return result


def load_idfa_feature_v2(country, signature, start_date, end_date, targetIDFA, tagMap, force=False):
    tmp_path = "{}/vector-{}-StartUTCDate={}-EndUTCDate={}.pkl".format(country, signature, start_date, end_date)
    if os.path.exists(tmp_path) and not force:
        return pkl.load(open(tmp_path, 'rb'))
    path = os.path.join(country, "vector", signature, "StartUTCDate=%s"%start_date, 'EndUTCDate=%s'%end_date)
    df_tmp = []
    dfs = []
    total = len(os.listdir(path))
    count = 0
    row = 0
    for filename in os.listdir(path):
        if filename.endswith('.parquet'):
            count += 1
            df = pd.read_parquet(os.path.join(path, filename), engine='pyarrow', columns=['idfaIndex'])
            LOGGER.info('total %d, loading num %d, len of df %d', total, count, len(df))
            max_idx = max(df['idfaIndex'].values)
            row = max_idx if max_idx > row else row
    row += 1
    column = len(tagMap.idx2item.keys())
    mat = lil_matrix((row, column), dtype=np.int8)
    LOGGER.info('row: %d, col %d', row, column)

    count = 0
    for filename in os.listdir(path):
        if filename.endswith('.parquet'):
            count += 1
            df = pd.read_parquet(os.path.join(path, filename), engine='pyarrow')
            LOGGER.info('total %d, loading num %d, len of df %d', total, count, len(df))
            for idx, row in df.iterrows():
                mat[row['idfaIndex'], row['vector']] = 1

    LOGGER.info('Start dump data')
    pkl.dump(mat, open(tmp_path, 'wb'))

    return dfs

def load_idfa_feature(country, signature, start_date, end_date, targetIDFA, force=False):
    tmp_path = "{}/vector-{}-StartUTCDate={}-EndUTCDate={}.pkl".format(country, signature, start_date, end_date)
    if os.path.exists(tmp_path) and not force:
        return pkl.load(open(tmp_path, 'rb'))
    path = os.path.join(country, "vector", signature, "StartUTCDate=%s"%start_date, 'EndUTCDate=%s'%end_date)
    df_tmp = []
    dfs = []
    total = len(os.listdir(path))
    count = 0
    for filename in os.listdir(path):
        if filename.endswith('.parquet'):
            count += 1
            df = pd.read_parquet(os.path.join(path, filename), engine='pyarrow')
            # df = df[df['idfaIndex'].isin(targetIDFA)]
            LOGGER.info('total %d, loading num %d, len of df %d', total, count, len(df))
            df_tmp.append(df)
            if count % 300 == 0 or count==(total-1):
                df_tmp = pd.concat(df_tmp)
                df_tmp = df_tmp[df_tmp['idfaIndex'].isin(targetIDFA)]
                df_tmp['vector'] = df_tmp['vector'].apply(list)
                df_tmp = df_tmp.groupby('idfaIndex').agg(sum).reset_index()
                dfs.append(df_tmp.copy())
                df_tmp = []
    LOGGER.info('final grouping')
    dfs = pd.concat(dfs)
    dfs['vector'] = dfs['vector'].apply(list)
    dfs = dfs.groupby('idfaIndex').agg(sum).reset_index()
    LOGGER.info('done group by')
    pkl.dump(dfs, open(tmp_path, 'wb'))
    return dfs

def load_token_map(country, signature, force=False):
    tmp_path = "{}/tokenMap-{}.pkl".format(country, signature)
    if os.path.exists(tmp_path) and not force:
        return pkl.load(open(tmp_path, 'rb'))
    path = os.path.join(country, "tokenMap", signature)
    df = pd.read_parquet(path, engine='pyarrow')
    try:
        tags = df['tag'].values
    except:
        tags = df['feature'].values
    tagMap = mapping(tags)
    tagMap.idx2item_v2 = {key:value.split('=')[0] for key, value in tagMap.idx2item.items()}
    types = list(set([t.split('/')[0] for t in tags]))
    typeMap = mapping(types)
    pkl.dump((tagMap, typeMap), open(tmp_path, 'wb'))
    return tagMap, typeMap

def load_filter(country, _os, osv, start_date, force=False):
    tmp_path = "{}/Filter-os={}-osv={}-StartUTCDate={}.pkl".format(country, _os, osv, start_date)
    if os.path.exists(tmp_path) and not force:
        return pkl.load(open(tmp_path, 'rb'))
    path = os.path.join(country, "tmp/Filter", "os=%s"%_os, "osv=%s"%osv, "StartUTCDate=%s"%start_date)
    df = pd.read_parquet(path, engine='pyarrow')
    targetIDFA = set(df['idfaIndex'].values)
    pkl.dump(targetIDFA, open(tmp_path, 'wb'))
    return targetIDFA

def load_filter_idfa(country, _os, osv, start_date, dir=None, force=False):
    if dir is not None:
        df = pd.read_parquet(dir, engine='pyarrow')
        targetIDFA = set(df['idfa'].values)
        return targetIDFA
    tmp_path = "{}/idfa-Filter-os={}-osv={}-StartUTCDate={}.pkl".format(country, _os, osv, start_date)
    if os.path.exists(tmp_path) and not force:
        return pkl.load(open(tmp_path, 'rb'))
    path = os.path.join(country, "tmp/Filter", "os=%s"%_os, "osv=%s"%osv, "StartUTCDate=%s"%start_date)
    df = pd.read_parquet(path, engine='pyarrow')
    targetIDFA = set(df['idfa'].values)
    pkl.dump(targetIDFA, open(tmp_path, 'wb'))
    return targetIDFA

def load_idfaMap(country, start_date, force=False):
    tmp_path = "{}/tmp-IDFA-StartUTCDate={}.pkl".format(country, start_date)
    if os.path.exists(tmp_path) and not force:
        return pkl.load(open(tmp_path, 'rb'))
    path = os.path.join(country, "tmp/IDFA", "StartUTCDate=%s" % start_date)
    df = pd.read_parquet(path, engine='pyarrow')
    idfaMap = mapping((df['idfa'].values, df['idfaIndex'].values))
    pkl.dump(idfaMap, open(tmp_path, 'wb'))
    return idfaMap

def dump_ranking_v2(tagMap, mat, feat='inventory'):
    actDict = defaultdict(int)
    for tag in tagMap.item2idx.keys():
        if tag.startswith(feat):
            try:
                weight = int(tag.split('=')[-1])
            except:
                weight = 1
            actDict[tagMap.idx2item_v2[tagMap.item2idx[tag]]] += weight * mat[:, tagMap.item2idx[tag]].sum()
    with open('t1.csv', 'w') as f:
        for key, value in sorted(actDict.items(), key=lambda x: x[1], reverse=True):
            f.write('{}, {}\n'.format(key, value))

def dump_ranking(tagMap, train_mat, filter, y_train_t1, y_train_t2, feat='inventory'):
    pos_train = train_mat[list(y_train_t1)]
    actDict = defaultdict(int)
    for tag in tagMap.item2idx.keys():
        if tag.startswith(feat):
            try:
                weight = int(tag.split('=')[-1])
            except:
                weight = 1
            actDict[tagMap.idx2item_v2[tagMap.item2idx[tag]]] += weight * pos_train[:, tagMap.item2idx[tag]].sum()
    with open('t1.csv', 'w') as f:
        for key, value in sorted(actDict.items(), key=lambda x: x[1], reverse=True):
            f.write('{}, {}\n'.format(key, value))

    pos_train = train_mat[list(y_train_t2)]

    actDict = defaultdict(int)
    for tag in tagMap.item2idx.keys():
        if tag.startswith(feat):
            try:
                weight = int(tag.split('=')[-1])
            except:
                weight = 1
            actDict[tagMap.idx2item_v2[tagMap.item2idx[tag]]] += weight * pos_train[:, tagMap.item2idx[tag]].sum()
    with open('t2.csv', 'w') as f:
        for key, value in sorted(actDict.items(), key=lambda x: x[1], reverse=True):
            f.write('{}, {}\n'.format(key, value))

    actDict = defaultdict(int)
    train_mat = train_mat[list(filter)]
    sum_mat = train_mat.sum(axis=0)
    for tag in tagMap.item2idx.keys():
        if tag.startswith(feat):
            try:
                weight = int(tag.split('=')[-1])
            except:
                weight = 1
            actDict[tagMap.idx2item_v2[tagMap.item2idx[tag]]] += weight * sum_mat[0, tagMap.item2idx[tag]].sum()
    with open('total.csv', 'w') as f:
        for key, value in sorted(actDict.items(), key=lambda x: x[1], reverse=True):
            f.write('{}, {}\n'.format(key, value))

def model_userlist(train_mat, filter, tagMap, idfaMap, rtagMap, model):
    r_mat, _ = gen_reduce_mat_v2(train_mat, tagMap, rtagMap)
    r_mat = r_mat.astype(dtype=np.float32)
    score = model.predict(r_mat)
    sortedidx = np.argsort(score)[::-1]

    mask = np.isin(sortedidx, list(filter))
    sortedidx = sortedidx[mask]

    userlist = []
    for idx in sortedidx:
        userlist.append(idfaMap.idx2item[idx])
    return userlist

def rule_userlist(train_mat, filter, tagMap, idfaMap, rules):
    usedtag = []
    for tag, idx in tagMap.item2idx.items():
        if tag.split('=')[0] in rules:
            usedtag.append(idx)

    train_mat = train_mat[:, usedtag]
    sum_mat = np.array(train_mat.sum(axis=1).reshape(1, -1))[0].astype(np.int32)
    sortedidx = np.argsort(sum_mat)[::-1]
    mask = np.isin(sortedidx, list(filter))
    sortedidx = sortedidx[mask]

    userlist = []
    for idx in sortedidx:
        userlist.append(idfaMap.idx2item[idx])
    return userlist

def rule_userlist_score_df(train_mat, filter, tagMap, idfaMap, rules):
    usedtag = []
    for tag, idx in tagMap.item2idx.items():
        if tag.split('=')[0] in rules:
            usedtag.append(idx)

    train_mat = train_mat[:, usedtag]
    sum_mat = np.array(train_mat.sum(axis=1).reshape(1, -1))[0].astype(np.int32)
    sortedidx = np.argsort(sum_mat)[::-1]
    mask = np.isin(sortedidx, list(filter))
    sortedidx = sortedidx[mask]

    userlist = []
    score = []
    for idx in sortedidx:
        userlist.append(idfaMap.idx2item[idx])
        score.append(sum_mat[idx])
    return pd.DataFrame({'idfa':userlist, 'score':score})