import pymysql


class MySQLDB:

    def __init__(self, rds_config):
        self.rds_config = rds_config
        self.connection = None

    def execute_query(self, query):
        with self.get_connection().cursor(pymysql.cursors.DictCursor) as cursor:
            result = cursor.execute(query)
        self.get_connection().commit()
        return result

    def execute_query_many(self, query, data):
        with self.get_connection().cursor(pymysql.cursors.DictCursor) as cursor:
            result = cursor.executemany(query, data)
        self.get_connection().commit()
        return result

    def get_query_result(self, query):
        with self.get_connection().cursor(pymysql.cursors.DictCursor) as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
        self.get_connection().commit()
        return result

    def get_connection(self):
        if not self.connection:
            self.connection = pymysql.connect(
                host=self.rds_config['host'],
                user=self.rds_config['username'],
                passwd=self.rds_config['password'],
                port=self.rds_config['port'],
                charset='utf8',
                db=self.rds_config['database']
            )
        return self.connection

    def close(self):
        if self.connection and self.connection.open:
            self.connection.close()
