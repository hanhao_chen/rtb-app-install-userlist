import json
import os.path
import pandas as pd

from utils.idash_utils import IDashUtils
from utils.mysql_utils import MySQLDB


def save_userlist(folder, list_name, user_ids):
    outfile = os.path.join(folder, list_name)
    with open(outfile, 'w') as fp:
        fp.write('\n'.join(user_ids) + '\n')


def save_userlist_csv(folder, list_name, user_ids_df):
    outfile = os.path.join(folder, list_name)
    user_ids_df.to_csv(outfile, index=False)


class UserListJobLog(IDashUtils):

    def __init__(self, config, table_name='bidcap_userlist_update_status', application='projectA'):
        super().__init__(config)
        self.table_name = table_name
        self.application = application
        self.deep_funnel_db = MySQLDB(config.get_mysql_database_credentials('ai_deep_funnel'))
        self.init_table()

    def init_table(self):
        query = """
        CREATE TABLE IF NOT EXISTS `{}` (
            `job_id` varchar(32) NOT NULL,
            `application` varchar(32) NOT NULL,
            `list_name` varchar(1024) NOT NULL,
            `action` varchar(32) NOT NULL,
            `job_status` varchar(32) NOT NULL DEFAULT 'pending',
            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`job_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(self.table_name)
        self.deep_funnel_db.execute_query(query)

    def record_job_id(self, job_id, list_name, action):
        query = "INSERT INTO {} (job_id, application, list_name, action) VALUES ('{}', '{}', '{}', '{}')".format(
            self.table_name, job_id, self.application, list_name, action)
        self.logger.info(query)
        self.deep_funnel_db.execute_query(query)

    def poll_job_status(self):
        results = {}
        query = "SELECT job_id FROM {} WHERE application = '{}' AND job_status = 'pending'".format(
            self.table_name, self.application)
        job_ids = [row['job_id'] for row in self.deep_funnel_db.get_query_result(query)]
        if not job_ids:
            return results

        jobs_status = self.read_userlist_job_ids(job_ids)
        assert jobs_status.get('success') and jobs_status.get('total_count', 0) == len(job_ids), \
            'query failed: {}'.format(jobs_status)

        for row in jobs_status['result']:
            if 'is_succeeded' not in row:
                status = 'pending'
            else:
                status = 'success' if row['is_succeeded'] else 'failed'
            results.setdefault(status, []).append(row)

        for status, rows in results.items():
            query = "UPDATE {} SET job_status = '{}' WHERE job_id in ({})".format(
                self.table_name, status, ','.join("'{}'".format(row['job_id']) for row in rows))
            self.logger.info(query)
            self.deep_funnel_db.execute_query(query)

        return results


class UserListServer(UserListJobLog):

    def __init__(self, config):
        super().__init__(config)
        self.list_server_host = config.get_list_server_credentials()
        self.list_name_to_id = self.get_list_name_to_id_mapping()

    def upload_user_list(self, list_name, user_ids):
        params = {
            'list_name': list_name,
            'generator_type': 'manual-upload',
            'modified_by': 'projectA',
            'site_id': 'share_site_id.appier',
            'is_update_idash_list': True,
            'list_destination': 'bidder_bloomfilter',
            'action': 'new'
        }
        list_id = self.list_name_to_id.get(list_name)
        if list_id:
            params['list_id'] = list_id
            params['action'] = 'replace'
        if params['action'] == 'new':
            params['id_types'] = json.dumps(['idfa', 'appier_cookie_uid'])
        files = {'list_data': '\n'.join(user_ids)}

        self.logger.info('%s list: %s with %s users', params['action'], params['list_name'], len(user_ids))
        response = self.send_post_request(
            self.list_server_host + '/generate/list', data=params, files=files, timeout=180).json()
        if not response.get('success', False):
            self.logger.error('update list failed: %s', params)
        self.logger.info(response)
