import logging

def logger(module_name, lvl=logging.INFO):
    ch = logging.StreamHandler()
    fmt = logging.Formatter(
        '[%(process)d][%(levelname)s] %(asctime)s - %(name)s.%(funcName)s:%(lineno)d - %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(fmt)
    logger = logging.getLogger(module_name)
    logger.setLevel(lvl)
    logger.addHandler(ch)
    return logger