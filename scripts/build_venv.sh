#!/bin/bash
BUILD_PATH=$1
REQUIREMENT_FILE=$2

cd $BUILD_PATH
if [ ! -d venv ]; then
    virtualenv venv --python=python3
fi
. venv/bin/activate
pip install -r $REQUIREMENT_FILE
deactivate
