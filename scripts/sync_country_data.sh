#!/bin/bash
if [ $# -eq 0 ]; then
	echo "No country"
	exit 1
fi
COUNTRY=$1
BUCKET=appier-ai-ai-agent-dev
FLAGS=""
echo syncing $COUNTRY ...
set -e
(ls $COUNTRY/tmp/IDFA || mkdir -p $COUNTRY/tmp/IDFA) && \
  aws s3 sync s3://$BUCKET/wush-wu/prepare-training/$COUNTRY/tmp/IDFA $COUNTRY/tmp/IDFA $FLAGS
(ls $COUNTRY/tmp/Filter || mkdir -p $COUNTRY/tmp/Filter) && \
  aws s3 sync s3://$BUCKET/wush-wu/prepare-training/$COUNTRY/tmp/Filter $COUNTRY/tmp/Filter $FLAGS
(ls $COUNTRY/rt3 || mkdir -p $COUNTRY/rt3) && \
  aws s3 sync s3://$BUCKET/wush-wu/prepare-training/$COUNTRY/rt3 $COUNTRY/rt3 $FLAGS
(ls $COUNTRY/tokenMap || mkdir -p $COUNTRY/tokenMap) && \
  aws s3 sync s3://$BUCKET/wush-wu/prepare-training/$COUNTRY/tokenMap $COUNTRY/tokenMap $FLAGS
(ls $COUNTRY/vector || mkdir -p $COUNTRY/vector) && \
  aws s3 sync s3://$BUCKET/wush-wu/prepare-training/$COUNTRY/vector $COUNTRY/vector $FLAGS

